# Использование ArchLinux



Менеджер пакетов используется __pacman__

Основноые команды:

    # обновляем список пакетов a-la "apt update"
    $ sudo pacman -Sy

    # обновляем уставноленные пакетов a-la "apt upgrade"
    $ sudo pacman -Syu

Установка пакета:

    # установка
    $ sudo pacman -S foo

Удаление пакета:

    $ sudo pacman -R foo

    # удаление с зависимостями
    $ sudo pacman -Rs foo

    # удаление с зависимостями и данными(включая конфигурацию) 
    $ sudo pacman -Rns foo

Установка AUR:

    # устанавливаем необходимые зависимости
    $ sudo pacman -S --needed git base-devel

    # клонируем репозиторий yay, собираем и запускаем
    $ git clone https://aur.archlinux.org/yay.git
    $ cd yay
    $ makepkg -si

    # проверяем установку
    $ yay --version

Использование AUR:

    # обновление всех пакетов в AUR
    $ yay -Syu

    # поиск пакета в AUR
    $ yay -Ss visual-studio-code

    # информация о пакете
    $ yay -Si visual-studio-code

Установка пакета:

    $ yay -S visual-studio-code

Удаление аналогично pacman:

    $ yay -R foo

    # удаление с зависимостями
    $ yay -Rs foo

    # удаление с зависимостями и данными(включая конфигурацию) 
    $ yay -Rns foo
