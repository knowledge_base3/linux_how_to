# How to install Arch Linux


Arch Linux поставляется без графического установзика и это вызывает проблематичность установки, т.к. непонятно что за чем должно быть и что с этим всем делать.

[Installation guide](https://wiki.archlinux.org/title/installation_guide_) из официального __wiw__ проясняет некоторые нюансы но не дает полной картины для понимания.

[Статься на хабре](https://habr.com/ru/articles/510158/) более менее понятная что зачем и почему

Множественные гайды из сети тоже в большинстве своем либо описывают процесс через какие то костыли, либо рассматривают какой то свой вариант, либо вообще древние и нерабочие инструкции.

Как и любую другую задачу, тем более если она непонятна или не была решена ранее, необходимо декомпозировать и разложить на этапы решения.

Для декомпозиции задачи обратимся к официальной __wiki__ и выделим основные этапы установки Arch Linux. Скачивание и запись на данный момент я упущу, и возможно добавлю в будущем.

Основные этапы установки заключаются в следующем:

1. Разметка дисков
2. Установка основных элементов системы
3. Установка графического окружения(при необходимости)


__Для запуска VMware в режиме UEFI необходимо в конец файла .vmx вписать *firmware = "efi"*__


# Разметка дисков

## UEFI

    # смотрим список дисков
    $ fdisk -l

    # переходим в настройку нужного диска
    $ cfdisk /dev/sda

Распределяем пространство дискаа примерно так:

![](<1.png>)

Выбираем Write >> yes >> Quit

## Форматирование дисков

    # UEFI
    $ mkfs.fat -F32 /dev/sda1

    # под систему
    $ mkfs.ext4 /dev/sda3

    # под Home
    $ mkfs.ext4 /dev/sda4


## Монтирование разделов

    # монтируем UEFI
    $ mkdir /mnt/efi
    $ mount /dev/sda1  /mnt/efi

    # монтируем корневой диск
    $ mount /dev/sda3  /mnt

    # монтируем Home
    $ mkdir /mnt/home
    $ mount /dev/sda4 /mnt/home

    # инициализируем и включаем swap
    
    $ mkswap /dev/sda2
    $ swapon /dev/sda2

На этом по идее работа с дисками законечна.

# Установка системы

В данный момент работа ведется с live-usb. 
Теперь необходимо установить систему на жесткий диск

    # установка базовой системы и утилит sudo и nano
    $ pacstrap -i /mnt base linux linux-firmware sudo nano

Если установка производится из старого дистрибутива то возможна примерно такая проблема:

    (129/129) checking keys in keyring
    downloading required keys...
    error: keyring is not writable
    error: keyring is not writable
    error: keyring is not writable

Чтобы ее исправить нужно:

    $ pacman-key --init
    $ pacman-key --populate archlinux

Далее будет предложено выбрать вариант __dbus__, __initfarms__. Я выбрал все по умолчанию и началось скачиваание нужных пакетов и их установка

# Настройка установленной системы

После установки нужно настроить систему до перезагрузки

    # Fstab
    $ genfstab -U /mnt >> /mnt/etc/fstab

    $ arch-chroot /mnt

    #Задайте часовой пояс:
    $ ln -sf /usr/share/zoneinfo/Регион/Город /etc/localtime

    # Запустите hwclock(8), чтобы сгенерировать /etc/adjtime:
    $ hwclock --systohc

    # задаем hostname
    $ echo hostname > /etc/hostname
    $ echo "127.0.0.1   localhost" >> /etc/hosts
    $ echo "::1         localhost" >> /etc/hosts
    $ echo "127.0.1.1   hostname.localdomain archlinux" >> /etc/hosts

    # устанавливаем сетевой менеджер
    $ pacman -S networkmanager
    $ systemctl enable NetworkManager 

    # установкаа пароля суперпользователя
    $ passwd

И последнее перед перезагрузкой это нужно установить GRUB в отведенный для него раздер UEFI: /dev/sda1

    $ pacman -S grub efibootmgr
    $ mkdir /boot/efi
    $ mount /dev/sda1 /boot/efi
    $ lsblk # для проверки всё ли норм смонтировано 
    $ grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi --removable
    $ grub-mkconfig -o /boot/grub/grub.cfg

Перезагружаемся

    $ exit
    $ umount -R /mnt
    $ reboot

# После перезагрузки

После перезагрузки у нас уже есть пользователь __root__. пароль тот который установили на щаге "__установкаа пароля суперпользователя__"

донастройка __swap__

    $ fallocate -l 3G /swapfile
    $ chmod 600 /swapfile
    $ mkswap /swapfile
    $ swapon /swapfile
    $ echo '/swapfile none swap sw 0 0' >> /etc/fstab

    # проверим подкключился ли swap
    $ free -m

Создадим пользователя

    $ useradd -m -g users -G wheel -s /bin/bash username
    $ passwd username

    $ nano /etc/sudoers
    # раскомментировать # %wheel ALL=(ALL) ALL или то что подходит

Устанавливаем необходимые пакеты для дальнейшего запуска графического окружения

    $ pacman -S pulseaudio pulseaudio-alsa xorg xorg-xinit xorg-server
    # будет предложено выбрать и по умолчанию all

Ну и наконец то установим графическое окружение рабочего стола:

    $ pacman -S xfce4 lightdm lightdm-gtk-greeter
    $ echo "exec startxfce4" > ~/.xinitrc
    $ systemctl enable lightdm

    # запустим графическое окружение
    $ startx

Либо можно поставить Gnome:

    $ sudo pacman -S gnome
    $ sudo pacman -S gdm

    # проверим что устаановилось
    $ pacman -Qs gdm

    # включаем GDM
    $ sudo systemctl enable --now gdm.service

    $ sudo reboot

Либо поставим KDEPlasma:

    # установит полную версиб KDE. При установке запросит параметры исталляции
    $ pacman -S plasma-meta

    # если нужно выбрать устанвливаемые пакеты 
    $ pacman -S plasma
    
    $ systemctl enable sddm
    $ reboot

теперь можно перезагрузиться и залогиниться под созданным пользователем
Я хз как это работает но Arch просто летает на виртуалке в сравнении даже с Debian12 установленного из netinst


