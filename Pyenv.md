# Pyenv

Инструмент для управления версиями Python. Для разработчика на Python этот инструмент must have.

[Домашняя страница Pyenv](https://github.com/pyenv/pyenv?tab=readme-ov-file#installation)

[Установщик Pyenv](https://github.com/pyenv/pyenv-installer)


## Install

    # короткая команда.
    # скачивается и запускается в bash скрипт для установки pyenv
    $ curl https://pyenv.run | bash

    # можно запустить сразу скрипт установки 
    $ curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash

По окончании установки в терминал будет выведено следующее предупреждение:

    WARNING: seems you still have not added 'pyenv' to the load path.

    # Load pyenv automatically by appending
    # the following to 
    # ~/.bash_profile if it exists, otherwise ~/.profile (for login shells)
    # and ~/.bashrc (for interactive shells) :

    export PYENV_ROOT="$HOME/.pyenv"
    [[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"

    # Restart your shell for the changes to take effect.

    # Load pyenv-virtualenv automatically by adding
    # the following to ~/.bashrc:

    eval "$(pyenv virtualenv-init -)"

Вкратце, тут предлагается настроить pyenv для автоматической его загрузки.

Можно воспользоваться предложением по настроке по умолчанию и добавить в __~/.bashrc__ следующее:

    $ nano ~/.bashrc

    # в самый конец вставляем:
    export PYENV_ROOT="$HOME/.pyenv"
    [[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"

Сохраняем. Перезапускаем терминал и проверяем что pyenv установлен:

    # вызов pyenv вернет версию, ккраткую инструкцию и список его доступных комманд
    $ pyenv
    pyenv 2.4.2
    Usage: pyenv <command> [<args>]

    Some useful pyenv commands are:
        activate    Activate virtual environment
        ...
        which       Display the full path to an executable

    See `pyenv help <command>' for information on a specific command.
    For full documentation, see: https://github.com/pyenv/pyenv#readme


# Using

    # версия
    $ pyenv version
    system (set by /home/ddd/.pyenv/version)

    # список установленных версий Python с отмекой активной версии
    $ pyenv versions
    * system (set by /home/ddd/.pyenv/version)

    # установка желаемой версии Python
    $ pyenv install 3.10
    Downloading Python-3.10.14.tar.xz...
    -> https://www.python.org/ftp/python/3.10.14/Python-3.10.14.tar.xz
    Installing Python-3.10.14...
    Installed Python-3.10.14 to /home/ddd/.pyenv/versions/3.10.14

    # удалить версию
    $ pyenv uninstall 3.10

    # вывод глобальной(системной) версии Python
    $ pyenv global
    system

    # включить нужную версию глобально(системно)
    $ pyenv global 3.10
    3.10

    # показать локальную версию(для активной дирректори)
    $ pyenv local
    pyenv: no local version configured for this directory

    # установить версию для дирректории. удобно для проекта, по идее сразу будет подтягиваться указанная версия только в этой дирректории
    $ pyenv local 3.10

    # теперь если создать и активировать __venv__ в этой дирректории
    $ python3 -m venv venv
    $ source venv/bin/activete

    # и вывести версию питона то получем назначенную для текущей дирректории вне зависимости какая версия установлена в системе и какая выбрана глобально.
    # главное чтобы назначенная версия Python для дирретокрии была установлена через pyenv install x.xx:
    $ python3 --version
    Python 3.10.14

# Troubleshooting

Возможнен такой результат установки нужной версии:

    $ pyenv install 3.10
    Downloading Python-3.10.14.tar.xz...
    -> https://www.python.org/ftp/python/3.10.14/Python-3.10.14.tar.xz
    Installing Python-3.10.14...

    BUILD FAILED (Debian 12 using python-build 20180424)

    Inspect or clean up the working tree at /tmp/python-build.20240612011135.3906
    Results logged to /tmp/python-build.20240612011135.3906.log

    Last 10 log lines:
    checking for python3... python3
    checking for --enable-universalsdk... no
    checking for --with-universal-archs... no
    checking MACHDEP... "linux"
    checking for gcc... no
    checking for cc... no
    checking for cl.exe... no
    configure: error: in `/tmp/python-build.20240612011135.3906/Python-3.10.14':
    configure: error: no acceptable C compiler found in $PATH
    See `config.log' for more details

__"configure: error: no acceptable C compiler found in $PATH"__ сообщает что нету в системе компилятора. Установим все необходимые пакеты.

    $ sudo apt install build-essential
    $ sudo apt install libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python3-openssl git

 В этом месте я не заморачивался пока что и установил то что выдал __ChatGPT__. Во время установки было предупреждение:

    Need to get 109 MB of archives.
    After this operation, 621 MB of additional disk space will be used.

Это конечно не очень хорошо, но я пока установил и pyenv заработал.



