# Uv 

[uv - PyPi](https://pypi.org/project/uv/)

Uv это пакетный менеджер для Pythin3
Синтаксис похож на стандартный __Pip__ но работает молниеносно.

Установка:

    $ curl -LsSf https://astral.sh/uv/install.sh | sh

В случае успеха видим:

    downloading uv 0.2.13 x86_64-unknown-linux-gnu
    installing to /home/user/.cargo/bin
    uv
    everything's installed!

    To add $HOME/.cargo/bin to your PATH, either restart your shell or run:

        source $HOME/.cargo/env (sh, bash, zsh)
        source $HOME/.cargo/env.fish (fish)

Перезапускае терминал, проходим в дирректорию проекта.
Для создания виртуального окружения пишум:

    $ uv venv

Создается Виртуальное окружение из текущей в ерсии Python.
Версию можно изменить с помощью [Pyenv](https://gitlab.com/knowledge_base3/linux_how_to/-/blob/main/Pyenv.md?ref_type=heads)

    # активируем окружение
    $ source .venv/bin/activate

Установка пакетов:

    $ uv pip install adafruit_ads1x15

или из __requirements.txt__

    $ uv pip install -r requirements.txt

    # или
    $ uv pip sync requirements.txt  # Install from a requirements.txt file

Фиксируем зависимости

    $ uv pip freeze > requirements.txt 

