При чистовой установке образа Debian может отсутствовать список доступных репозиториев и уставнолено чтение только с cd-rom.
Соответственно apt работать будет только в режиме чтения с cd-rom.

чтобы это сиправить необходимо дополнить sources.list следующимобразом:

    $ sudo nano /etc/apt/sourced.list

Комментируем запись:

    deb cdrom:*

Для получения стабильных версий пакетов вставляем этот список в sources.list:

    deb https://ftp.debian.org/debian/ bookworm contrib main non-free non-free-firmware
    # deb-src https://ftp.debian.org/debian/ bookworm contrib main non-free non-free-firmware

    deb https://ftp.debian.org/debian/ bookworm-updates contrib main non-free non-free-firmware
    # deb-src https://ftp.debian.org/debian/ bookworm-updates contrib main non-free non-free-firmware

    deb https://ftp.debian.org/debian/ bookworm-proposed-updates contrib main non-free non-free-firmware
    # deb-src https://ftp.debian.org/debian/ bookworm-proposed-updates contrib main non-free non-free-firmware

    deb https://ftp.debian.org/debian/ bookworm-backports contrib main non-free non-free-firmware
    # deb-src https://ftp.debian.org/debian/ bookworm-backports contrib main non-free non-free-firmware

    deb https://security.debian.org/debian-security/ bookworm-security contrib main non-free non-free-firmware
    # deb-src https://security.debian.org/debian-security/ bookworm-security contrib main non-free non-free-firmware


Для получения более новых версий пакетов заменяем __bookworm__ на желаемую ветку, например __testing__. В таком случае пакеты будут имет более новые версии из ветки __testing__:
    
    deb https://ftp.debian.org/debian/ testing contrib main non-free non-free-firmware
    # deb-src https://ftp.debian.org/debian/ testing contrib main non-free non-free-firmware

    deb https://ftp.debian.org/debian/ testing-updates contrib main non-free non-free-firmware
    # deb-src https://ftp.debian.org/debian/ testing-updates contrib main non-free non-free-firmware

    deb https://ftp.debian.org/debian/ testing-proposed-updates contrib main non-free non-free-firmware
    # deb-src https://ftp.debian.org/debian/ testing-proposed-updates contrib main non-free non-free-firmware

    deb https://ftp.debian.org/debian/ testing-backports contrib main non-free non-free-firmware
    # deb-src https://ftp.debian.org/debian/ testing-backports contrib main non-free non-free-firmware

    deb https://security.debian.org/debian-security/ testing-security contrib main non-free non-free-firmware
    # deb-src https://security.debian.org/debian-security/ testing-security contrib main non-free non-free-firmware


Сохраняем файл и обновляем список пакетов и обновляем систему:

    $ sudo apt update
    $ sudo apt upgrade -Y