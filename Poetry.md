# Poetry

Менеджер пакетов вместо pip.

[домашняя страница](https://python-poetry.org/)


## [Install](https://python-poetry.org/docs/#installing-with-the-official-installer)

Официальный установщик:

    $ curl -sSL https://install.python-poetry.org | python3 -
    Retrieving Poetry metadata
    # Welcome to Poetry!
    This will download and install the latest version of Poetry,
    a dependency and package manager for Python.
    It will add the `poetry` command to Poetry's bin directory, located at:
    $HOME/.local/bin
    You can uninstall at any time by executing this script with the --uninstall option,
    and these changes will be reverted.
    Installing Poetry (1.8.3): Done
    Poetry (1.8.3) is installed now. Great!
    To get started you need Poetry's bin directory (/home/ddd/.local/bin) in your `PATH`
    environment variable.
    Add `export PATH="/home/ddd/.local/bin:$PATH"` to your shell configuration file.
    Alternatively, you can call Poetry explicitly with `/home/ddd/.local/bin/poetry`.
    You can test that everything is set up by executing:
    `poetry --version`

Выполним рекомендацию __"Add \`export PATH="$HOME/.local/bin:$PATH"` to your shell configuration file."__ для того чтобы команда __poetry__ работала. Для этого в bashrc делаем экспорт:

    $ nano ~/.bashrc

    # и в самый конец вставляемЖ
    # poetry
    export PATH="$HOME/.local/bin:$PATH"

Перезапускаем терминал и прверим версию:

    $ poetry --version

## [Using](https://python-poetry.org/docs/cli/)

    # для нового проекта необходимо его инициализировать
    $ poetry install

    # установка зависиимостей из project.toml
    $ poetry install

    # обновление зависимостей. 
    $ poetry update

    # установить нужный пакет
    $ poetry add requests pendulum

    # установить нужный пакет с указанием версии не ниже определенной
    $ poetry add "pendulum>=2.0.5"

    # установить фиксированную версию пакета
    $ poetry add pendulum==2.0.5
    









